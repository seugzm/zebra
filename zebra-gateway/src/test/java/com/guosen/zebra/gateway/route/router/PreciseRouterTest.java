package com.guosen.zebra.gateway.route.router;

import com.guosen.zebra.gateway.route.cache.ConcreteUrlRouteCache;
import com.guosen.zebra.gateway.route.model.RouteInfo;
import mockit.Expectations;
import mockit.Mocked;
import mockit.Tested;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.nullValue;
import static org.hamcrest.Matchers.is;

public class PreciseRouterTest {

    @Tested
    private PreciseRouter preciseRouter;

    @Mocked
    private ConcreteUrlRouteCache concreteUrlRouteCache;

    /**
     * 测试因没有路由信息而导致的不匹配情况
     */
    @Test
    public void testNotMatchForNoRouteInfo() {

        new Expectations() {
            {
                concreteUrlRouteCache.getUrlRouteInfoMap();
                result = new HashMap<>();
            }
        };

        RouteInfo resultRouteInfo = preciseRouter.route("/firstService/hello");

        assertThat(resultRouteInfo, is(nullValue()));
    }

    @Test
    public void testNotMatch() {

        Map<String, RouteInfo> concreteUrlRouteInfoMap = new HashMap<>();
        concreteUrlRouteInfoMap.put("/firstService/echo", RouteInfo.newBuilder().build());

        new Expectations() {
            {
                concreteUrlRouteCache.getUrlRouteInfoMap();
                result = concreteUrlRouteInfoMap;
            }
        };

        RouteInfo resultRouteInfo = preciseRouter.route("/firstService/hello");

        assertThat(resultRouteInfo, is(nullValue()));
    }

    /**
     * 测试匹配到的情况
     */
    @Test
    public void testMatch() {
        RouteInfo routeInfo = RouteInfo.newBuilder()
                .method("hello")
                .serviceName("com.guosen.zebra.sample.FirstService")
                .build();

        Map<String, RouteInfo> concreteUrlRouteInfoMap = new HashMap<>();
        concreteUrlRouteInfoMap.put("/firstService/hello", routeInfo);

        new Expectations() {
            {
                concreteUrlRouteCache.getUrlRouteInfoMap();
                result = concreteUrlRouteInfoMap;
            }
        };

        RouteInfo resultRouteInfo = preciseRouter.route("/firstService/hello");

        assertThat(resultRouteInfo, is(routeInfo));
    }

}
