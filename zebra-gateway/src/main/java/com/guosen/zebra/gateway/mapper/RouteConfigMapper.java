package com.guosen.zebra.gateway.mapper;

import com.guosen.zebra.gateway.route.model.RouteConfig;

import java.util.List;

/**
 * 网关路由规则 Mapper 接口
 */
public interface RouteConfigMapper {

    /**
     * 获取网关路由规则配置列表
     * @return 网关路由规则配置列表
     */
    List<RouteConfig> getConfigs();
}
